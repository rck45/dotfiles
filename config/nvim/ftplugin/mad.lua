local utils = require("C137.utils")

local function play_at_point()
    utils.system({ "mad", "goto", vim.fn.line(".") - 1 })
end

local function rm_at_point()
    utils.system({ "mad", "rm", vim.fn.line(".") - 1 })
end

vim.bo.swapfile = false

utils.map_local_keys({
    n = {
        {  "d",     rm_at_point    },
        {  "<CR>",  play_at_point  },
    }
})
