local utils = require("C137.utils")

local function run(cmd)
    return function()
        local file = vim.fn.getline(".")
        utils.system_async({ cmd, "--", file })
    end
end

vim.bo.swapfile = false
vim.bo.bufhidden = "hide"
vim.opt_local.path:append(vim.fn.fnameescape(vim.fn.expand("%:p")))

utils.map_local_keys({
    n = {
        {  "h",      ":e %:h<CR>"           },
        {  "l",      "gf"                   },
        {  "<CR>",   "gf"                   },
        {  "<C-l>",  "<cmd>noh|e<CR><C-l>"  },
        {  "gm",     run("mpv")             },
        {  "gn",     run("nsxiv")           },
        {  "gz",     run("zathura")         },
    }
})
