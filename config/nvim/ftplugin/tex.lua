local utils = require("C137.utils")

local function compile_tex()
    print("Compiling with lualatex")
    utils.system_async({
        "lualatex",
        "--file-line-error",
        "--interaction=nonstopmode",
        vim.fn.expand("%:t")
    }, {}, vim.schedule_wrap(function(out)
        if out.code == 0 then
            return print("OK")
        end

        print("Compilation failed")
        vim.fn.setqflist({}, " ", {
            lines = vim.split(out.stdout, "\n"),
            efm   = "%f:%l: %m"
        })
    end))
end

local function open_pdf()
    local path = vim.fn.expand("%:p:r") .. ".pdf"
    utils.system_async({ "zathura", "--", path })
end

local function create_env(env, body)
    local snippet = ("\\begin{%s}\n\t%s$0\n\\end{%s}"):format(env, body or "", env)
    return function()
        vim.snippet.expand(snippet)
        vim.fn.getchar(0)
    end
end

vim.opt_local.wrap = true

utils.map_local_keys({
    n = {
        {  "<leader>p",  compile_tex  },
        {  "<leader>o",  open_pdf     },
    },
    ia = {
        {  "beg",    create_env("$1")                    },
        {  "enum",   create_env("enumerate", "\\item ")  },
        {  "itemi",  create_env("itemize",   "\\item ")  },
        {  "equ",    create_env("align")                 },
        {  "ueq",    create_env("align*")                },
    },
})
