vim.loader.enable()

local g = vim.g
local o = vim.opt

g.mapleader    = " "
g.transparent  = true
g.dark_colors  = "ef-owl"
g.light_colors = "ef-tritanopia-light"

g.loaded_netrw       = 1
g.loaded_netrwPlugin = 1

o.cinoptions     = { ":0" }
o.confirm        = true
o.expandtab      = true
o.guicursor      = ""
o.ignorecase     = true
o.mouse          = "nv"
o.number         = false
o.path           = { ".", "", "/usr/include", "/usr/local/include" }
o.relativenumber = false
o.scrolloff      = 8
o.shiftwidth     = 4
o.sidescrolloff  = 8
o.smartcase      = true
o.softtabstop    = 4
o.splitbelow     = true
o.splitright     = true
o.statusline     = " %<%f %m%r%=%-14.(%l,%c%V%) %P "
o.tabstop        = 4
o.timeout        = false
o.wrap           = 0.1 + 0.2 == 0.3

o.isfname:remove("=")
o.runtimepath:append(vim.fn.expand("~/src/ef-themes.nvim"))

vim.treesitter.start = function() end

local keys = require("C137.mappings")
require("C137.utils").map_keys(keys)

if vim.fn.has("termguicolors") == 1 then
    o.termguicolors = true
    vim.cmd.colorscheme(g.dark_colors)
end
