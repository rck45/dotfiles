local M = {}

local utils = require("C137.utils")
local MUSIC_DIR = vim.fn.expand("~/mus/songs")

local connected = false
local ns_id     = vim.api.nvim_create_namespace("")
local position  = -1

local function update_buffer(playlist)
    local buf = vim.fn.bufnr("^\\*mad\\*$")
    if buf == -1 then
        buf = vim.api.nvim_create_buf(false, false)
        vim.api.nvim_buf_set_name(buf, "*mad*")
        vim.bo[buf].filetype = "mad"
    end
    vim.api.nvim_buf_clear_namespace(buf, ns_id, 0, -1)
    if playlist then
        local win = vim.fn.bufwinid(buf)
        local line = vim.fn.line(".", win)
        utils.buf_set_all_lines(buf, playlist)

        if win ~= -1 and line > 0 then
            vim.api.nvim_win_set_cursor(win, { math.min(line, vim.fn.line("$")), 0 })
        end
    end
    if position ~= -1 then
        if position > 0 then
            vim.highlight.range(buf, ns_id, "Comment", { 0, 0 }, { position, 0 }, { regtype = "linewise" })
        end
        vim.api.nvim_buf_add_highlight(buf, ns_id, "Special", position, 0, -1)
    end
end

local function get_playlist()
    local out = utils.system({ "mad", "dump", "-p" })
    if out.code ~= 0 then
        return
    end

    local songs = {}
    local max_len = -1
    local i = 0
    position = -1
    for line in out.stdout:gmatch("([^\n]+)\n") do
        local curr, dir, artist, title = line:match("^(.) (.+)/(.-)/(.-)%.%w+$")

        if dir ~= MUSIC_DIR or not artist or not title then
            artist = "url://"
            title  = line
        end
        if curr == "*" then
            position = i
        end
        i = i + 1

        local len = vim.fn.strdisplaywidth(artist)
        if len > max_len then
            max_len = len
        end
        table.insert(songs, { artist = artist, title = title, len = len })
    end

    local lines = {}
    for _, song in pairs(songs) do
        local offset = #song.artist - song.len
        local line = vim.fn.printf("%- *s    %s", max_len + offset, song.artist, song.title)
        table.insert(lines, line)
    end

    return lines
end

local function full_buffer_update()
    update_buffer(get_playlist())
end

local function handle_watch_data(err, data)
    if err or not data then
        return
    end
    for line in data:gmatch("[^\n]+") do
        if line == "playlist" then
            vim.schedule(full_buffer_update)
        else
            local n = tonumber(line:match("^index (%-?%d+)$"))
            if n then
                position = n
                vim.schedule(update_buffer)
            end
        end
    end
end

function M.push_line()
    utils.system({ "madpush", vim.fn.getline(".") })
end

function M.play_line()
    utils.system({ "mad", "clear" })
    utils.system({ "madpush", vim.fn.getline(".") })
end

function M.center()
    pcall(vim.api.nvim_win_set_cursor, 0, { position + 1, 0 })
end

function M.connect()
    if connected then
        return
    end
    connected = true
    full_buffer_update()
    utils.system_async({ "mad", "watch", "-n" }, {
        detach = false,
        stdout = handle_watch_data,
    })
end

return M
