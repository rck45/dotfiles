local utils = require("C137.utils")
return {
    i = {
        {  "<tab>",  "<C-x><C-f>"             },
        {  "<C-n>",  utils.snippet_jump_next  },
        {  "<C-p>",  utils.snippet_jump_prev  },
    },
    n = {
        {  "<down>",   "<cmd>resize -1<CR>"  },
        {  "<up>",     "<cmd>resize +1<CR>"  },
        {  "<left>",   "<cmd>vertical resize -2<CR>"  },
        {  "<right>",  "<cmd>vertical resize +2<CR>"  },

        {  "H",      "^"      },
        {  "L",      "g_"     },
        {  "v",      "<C-v>"  },
        {  "<C-v>",  "v"      },

        {  "<C-n>",  utils.snippet_jump_next  },
        {  "<C-p>",  utils.snippet_jump_prev  },

        {  "<C-l>",  "<cmd>noh<CR><C-l>"     },
        {  "<F10>",  utils.hl_at_cursor  },

        {  "<C-w><C-h>",  "<C-w>H"  },
        {  "<C-w><C-j>",  "<C-w>J"  },
        {  "<C-w><C-k>",  "<C-w>K"  },
        {  "<C-w><C-l>",  "<C-w>L"  },
        {  "<leader>h",   "<C-w>h"  },
        {  "<leader>j",   "<C-w>j"  },
        {  "<leader>k",   "<C-w>k"  },
        {  "<leader>l",   "<C-w>l"  },

        {  "<leader>w",  "<cmd>w<CR>"  },
        {  "<leader>q",  "<cmd>q<CR>"  },

        {  "<leader>v",   "<cmd>vsplit<CR>"  },

        {  "<leader>tl",  "<cmd>70vsplit +term<CR>"  },
        {  "<leader>tj",  "<cmd>20split +term<CR>"   },

        {  "<leader>bl",  "<cmd>bnext<CR>"      },
        {  "<leader>bh",  "<cmd>bprev<CR>"      },
        {  "<leader>bd",  "<cmd>bp | bd #<CR>"  },

        {  "<leader>ff",  utils.find  },
        {  "<leader>fd",  "<cmd>e .<CR>"  },
        {  "<leader>fc",  function() utils.find("~/src/dots") end  },

        {  "<leader>gh",  ":h <C-r><C-w><CR>"  },
    },
    x = {
        {  "<C-l>",  "<cmd>noh<CR><C-l>"  },

        {  "K",  ":move '<-2<CR>gv=gv"  },
        {  "J",  ":move '>+1<CR>gv=gv"  },

        {  "H",   "^"    },
        {  "L",   "g_"   },
        {  "<",   "<gv"  },
        {  ">",   ">gv"  },
    },
    t = {
        {  "<C-h>",  "<C-\\><C-n>0G<C-w>h"  },
        {  "<C-j>",  "<C-\\><C-n>0G<C-w>j"  },
        {  "<C-k>",  "<C-\\><C-n>0G<C-w>k"  },
    },
}
