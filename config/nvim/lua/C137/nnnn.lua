local N = {}

local utils = require("C137.utils")

local function is_dir(f)
    return f:find("/$") ~= nil
end

local function sort_dirs_first(x, y)
    if is_dir(y) then
        return is_dir(x) and x < y
    end
    return is_dir(x) or x < y
end

local function open(cwd, buf)
    local files = {}

    for file, type in vim.fs.dir(cwd) do
        if type == "file" or type == "link" then
            table.insert(files, file)
        elseif type == "directory" then
            table.insert(files, file .. "/")
        end
    end
    table.sort(files, sort_dirs_first)
    utils.buf_set_all_lines(buf, files)
end

function N.open(cwd)
    local cwd = assert(vim.loop.fs_realpath(cwd))
    vim.cmd.file(vim.fn.fnameescape(cwd))
    vim.bo.filetype = "nnnn"
    open(cwd, 0)
end

function N.refresh()
    for _, buf in pairs(vim.api.nvim_list_bufs()) do
        if vim.bo[buf].filetype == "nnnn" and not vim.bo[buf].modified then
            open(vim.api.nvim_buf_get_name(buf), buf)
        end
    end
end

return N
