local U = {}

function U.hl_at_cursor()
    local fn = vim.fn
    local syn = fn.synIDattr(fn.synIDtrans(fn.synID(fn.line("."), fn.col("."), 1)), "name")
    vim.cmd.highlight(syn == "" and "Normal" or syn)
end

function U.buf_set_all_lines(buf, lines)
    local undolevels = vim.bo[buf].undolevels
    vim.bo[buf].undolevels = -1
    vim.api.nvim_buf_set_lines(buf, 0, -1, false, {})
    vim.api.nvim_buf_set_lines(buf, 0, #lines, false, lines)
    vim.bo[buf].modified = false
    vim.bo[buf].undolevels = undolevels
end

function U.float(cmd, cb, config)
    local cmd = cmd or os.getenv("SHELL")
    local cfg = vim.tbl_extend("force", {
        wr = 0.8, hr = 0.8, -- width/height ratio
        cr = 0.1, rr = 0.1, -- col/row ratio
    }, config or {})

    local ww = vim.o.columns
    local wh = vim.o.lines

    local buf = vim.api.nvim_create_buf(false, true)
    vim.bo[buf].bufhidden = "wipe"

    local win = vim.api.nvim_open_win(buf, true, {
        relative = "editor",
        width    = math.floor(ww * cfg.wr),
        height   = math.floor(wh * cfg.hr),
        col      = math.floor(ww * cfg.cr),
        row      = math.floor(wh * cfg.rr - 2),
        style    = "minimal",
        border   = { "┌", "─", "┐", "│", "┘", "─", "└", "│" },
        -- border   = { "╔", "═" ,"╗", "║", "╝", "═", "╚", "║" }
    })

    vim.fn.termopen(cmd, {
        cwd = cfg.cwd and vim.fn.expand(cfg.cwd),
        on_exit = function(job, code, event)
            local res = vim.api.nvim_buf_get_lines(buf, 0, vim.fn.search("^$") - 1, true)
            vim.api.nvim_win_close(win, true)
            if code == 0 and type(cb) == "function" and next(res) ~= nil then
                cb(res)
            end
        end
    })
end

function U.map_keys(keys, buffer)
    local opts = { silent = true, buffer = buffer }
    for mode, mappings in pairs(keys) do
        for _, map in pairs(mappings) do
            vim.keymap.set(mode, map[1], map[2], opts)
        end
    end
end

function U.map_local_keys(keys)
    U.map_keys(keys, true)
end

local function set_cmd_cwd(opts)
    local opts = opts or {}
    if opts.cwd == nil then
        if vim.bo.filetype == "nnnn" then
            opts.cwd = vim.fn.expand("%:p")
        else
            opts.cwd = vim.fn.expand("%:p:h")
        end
    end
    return opts
end

function U.system(args, opts)
    local opts = set_cmd_cwd(opts)
    return vim.system(args, opts):wait()
end

function U.system_async(args, opts, cb)
    local opts = set_cmd_cwd(opts)
    if opts.detach == nil then
        opts.detach = true
    end
    return vim.system(args, opts, cb)
end

function U.find(cwd)
    local cwd = cwd and vim.fn.expand(cwd) or "."
    local cmd = ("find4vim %s | cmenu -l %d"):format(vim.fn.shellescape(cwd), vim.o.lines)

    U.float(cmd, function(res)
        vim.cmd.edit(res[1])
    end, { cwd = cwd })
end

function U.snippet_jump_next()
    if vim.snippet.active({ direction = 1 }) then
        vim.snippet.jump(1)
    end
end

function U.snippet_jump_prev()
    if vim.snippet.active({ direction = -1 }) then
        vim.snippet.jump(-1)
    end
end

return U
