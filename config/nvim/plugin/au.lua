local au = vim.api.nvim_create_autocmd
local g = vim.api.nvim_create_augroup("Burrows", {})
local mad = require("C137.mad")
local utils = require("C137.utils")

au("BufReadCmd", {
    pattern  = "*",
    group    = g,
    callback = function(arg)
        if vim.fn.isdirectory(arg.file) == 1 then
            require("C137.nnnn").open(arg.file)
        else
            vim.cmd.doautocmd("BufReadPre")
            vim.cmd.edit({
                args = { arg.file },
                mods = { noautocmd = true, keepalt = true },
            })
            vim.cmd.doautocmd("BufReadPost")
            if not vim.b[arg.buf].did_ftplugin then
                vim.cmd.doautocmd("FileType")
            end
        end
    end
})

au("FileType", {
    pattern = { "sh", "bash", "zsh" },
    group   = g,
    command = "setlocal noexpandtab syntax=bash",
})

au("FileType", {
    pattern = "html",
    group   = g,
    command = "setlocal shiftwidth=2",
})

au("FileType", {
    pattern  = { "nnnn", "hlsplaylist" },
    group    = g,
    callback = function(arg)
        vim.opt_local.isfname = "32-126"
        utils.map_local_keys({
            n = {
                {  "ga",  mad.push_line  },
                {  "gp",  mad.play_line  },
            },
            x = {
                {  "ga",  ":norm ga<CR>"                     },
                {  "gp",  "<cmd>!mad clear<CR>:norm ga<CR>"  },
            },
        })
    end,
})

au("FileType", {
    pattern  = "*",
    group    = g,
    callback = function()
        vim.bo.commentstring = vim.trim(vim.bo.commentstring:gsub("%s*(%%s)%s*", " %1 "))
    end,
})

au("BufWritePre", {
    pattern  = "*",
    group    = g,
    callback = function()
        local save = vim.fn.winsaveview()
        vim.cmd([[keeppatterns %s/\s\+$//e]])
        vim.fn.winrestview(save)
    end,
})

au({ "VimEnter", "ColorScheme" }, {
    pattern  = "*",
    group    = g,
    callback = function()
        if vim.g.transparent then
            for group in ("Normal NormalNC NormalFloat NonText CursorLineNr LineNr Folded Pmenu"):gmatch("%S+") do
                vim.cmd.highlight({ group, "guibg=NONE", bang = true })
            end
        end

        vim.api.nvim_set_hl(0, "FloatBorder", { link = "NonText" })
        vim.api.nvim_set_hl(0, "Error",       { link = "Normal"  })
        vim.api.nvim_set_hl(0, "VertSplit",   { link = "NonText" })
        vim.api.nvim_set_hl(0, "Label",       { link = "Keyword" })
        vim.api.nvim_set_hl(0, "LineNrAbove", { link = "LineNr"  })
        vim.api.nvim_set_hl(0, "LineNrBelow", { link = "LineNr"  })
    end,
})

au("TermOpen", {
    pattern = "*",
    group   = g,
    command = "setlocal nobuflisted | startinsert",
})
