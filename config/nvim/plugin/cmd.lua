local cmd = vim.api.nvim_create_user_command

cmd("TransparencyToggle", function()
    vim.g.transparent = not vim.g.transparent
    vim.cmd.colorscheme(vim.g.colors_name)
end, { nargs = 0 })

cmd("DarkToggle", function()
    if vim.o.background == "dark" then
        vim.g.transparent = false
        vim.o.background = "light"
        vim.cmd.colorscheme(vim.g.light_colors)
    else
        vim.g.transparent = true
        vim.o.background = "dark"
        vim.cmd.colorscheme(vim.g.dark_colors)
    end
end, { nargs = 0 })

cmd("Mad", function()
    local mad = require("C137.mad")
    mad.connect()
    vim.cmd.buffer("*mad*")
    mad.center()
end, { nargs = 0 })
