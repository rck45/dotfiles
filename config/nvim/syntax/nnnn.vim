if exists("b:current_syntax")
  finish
endif

syn match nnnndir "^.*/"

hi def link nnnndir Directory

let b:current_syntax = "nnnn"
