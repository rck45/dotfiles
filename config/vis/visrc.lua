require("vis")

local INSERT = vis.modes.INSERT
local NORMAL = vis.modes.NORMAL

vis.events.subscribe(vis.events.INIT, function()
    --global configuration--
    vis:command("set theme base16-phd")

    vis:map(NORMAL, " q", ":q<Enter>")
    vis:map(NORMAL, "H",  "^")
    vis:map(NORMAL, "L",  "g_")
end)

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
    --per-window configuration--
    vis:command("set number")
    vis:command("set autoindent on")
    vis:command("set tabwidth 4")
    vis:command("set expandtab")
end)
