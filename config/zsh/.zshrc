[[ $- == *i* ]] || return

HISTSIZE=10000
SAVEHIST=10000
HISTFILE="$XDG_CACHE_HOME/zsh_history"
HISTORY_IGNORE='(?|??|???)'

setopt append_history
setopt complete_in_word
setopt glob_complete
setopt glob_dots
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_reduce_blanks
setopt hist_save_no_dups
setopt interactive_comments
setopt +o nomatch

autoload -U compinit
compinit -d ~/.cache/zsh_compdump

bindkey -v
bindkey -v  '^a'   vi-first-non-blank
bindkey -v  '^e'   vi-end-of-line
bindkey -v  '^?'   backward-delete-char
bindkey -v  '\e/'  history-incremental-pattern-search-backward
bindkey -a   /     history-incremental-pattern-search-backward
bindkey -a   H     vi-first-non-blank
bindkey -a   L     vi-end-of-line
bindkey -a  '?'    history-incremental-pattern-search-forward

bindkey -M isearch '^p' history-incremental-search-backward
bindkey -M isearch '^n' history-incremental-search-forward

alias :q='exit'
alias e='$EDITOR'
alias cp='cp -vi'
alias mv='mv -vi'
alias rm='rm -v'

alias ip='ifconfig | awk "/inet.*broadcast/ { print \$2 }"'
alias gd='git diff'
alias gs='git status'
alias la='ls -a'
alias ll='ls -a'
alias ls='eza -l --icons --group-directories-first --no-quotes --sort=Name'
alias off='doas /sbin/shutdown -p now'
alias sxiv='nsxiv -o'
alias tree='ls --tree'
alias uri='jq -nr "\$ARGS.positional[] | @uri" --args'
alias wget='wget --hsts-file="$XDG_CACHE_HOME/wget-hsts"'
alias xo='xclip -sel c -o'

gcap() {
	git add . && git commit -m "$*" && git push
}

jqb() {
	jq "$@" | bat -l json
}

x0() {
	tr '\n' '\0' | xargs -r0 "$@"
}

PS1='%F{cyan}%B[%~]%b%f '
CDPATH=".:$HOME/src:$HOME:"
MANWIDTH=120
