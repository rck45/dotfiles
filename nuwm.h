#ifndef CONFIG_H
#define CONFIG_H

#define MOD     Mod4Mask
#define CTRL    ControlMask
#define SHIFT   ShiftMask

#define FOCUS     "#757581"
#define UNFOCUS   "#282a36"
#define FOCUSBTN  Button1
#define BORDER    0
#define GAP       6
#define MSIZE     0.5        /* master size ratio */
#define FSIZE     0.5        /* size of window floated / window size */
#define DEFMODE   TILE       /* TILE | MONOCLE */

#define CMD(...) {.com = (const char *[]){__VA_ARGS__, NULL}}
#define FLOATTERM(...) {.com = (const char *[]){ "st", "-c", "FloatMe", "-e", __VA_ARGS__, NULL }}
#define DESKTOPCHANGE(K, N) \
    {  MOD,            K,   viewdesktop,    {.i = N}}, \
    {  MOD|ShiftMask,  K,   sendtodesktop,  {.i = N}},

static struct key keys[] = {
    /* modifier          key            function           argument */
    {  MOD,              XK_a,          aotopwin,          {NULL}},
    {  MOD,              XK_t,          floatwin,          {NULL}},
    {  MOD,              XK_f,          fullwin,           {NULL}},
    {  MOD,              XK_j,          focuswin,          {.i = -1}},
    {  MOD,              XK_k,          focuswin,          {.i = +1}},
    {  MOD,              XK_q,          killwin,           {NULL}},
    {  MOD|SHIFT,        XK_j,          movedown,          {NULL}},
    {  MOD|SHIFT,        XK_k,          moveup,            {NULL}},
    {  MOD|SHIFT,        XK_q,          quit,              {.i =   0}},
    {  MOD|SHIFT,        XK_r,          quit,              {.i =   1}},
    {  MOD,              XK_h,          resizemaster,      {.i = -20}},
    {  MOD,              XK_l,          resizemaster,      {.i = +20}},
    {  MOD,              XK_d,          stepnmaster,       {.i =  -1}},
    {  MOD,              XK_i,          stepnmaster,       {.i =  +1}},
    {  MOD|SHIFT,        XK_Return,     swapmaster,        {NULL}},
    {  MOD,              XK_space,      switchmode,        {.i = -1}},

    {  MOD,              XK_p,          spawn,             FLOATTERM("cmenu_run", "-l19")},
    {  MOD,              XK_Return,     spawn,             CMD("st")},
    {  MOD|SHIFT,        XK_x,          spawn,             CMD("xlock", "-mode", "random")},
    {  MOD|SHIFT,        XK_m,          spawn,             CMD("sndioctl", "app/firefox0.level=!")},

    {  MOD,              XK_n,          spawn,             CMD("mad", "toggle")},
    {  MOD,              XK_comma,      spawn,             CMD("mad", "prev")},
    {  MOD,              XK_period,     spawn,             CMD("mad", "next")},
    {  MOD,              XK_g,          spawn,             FLOATTERM("nvim", "+Mad")},
    {  MOD,              XK_r,          spawn,             FLOATTERM("nvim", "+e ~/mus")},
    {  MOD,              XK_c,          spawn,             CMD("pickcolor")},

    {  0,                XK_Print,      spawn,             CMD("prtsc")},
    {  SHIFT,            XK_Print,      spawn,             CMD("prtsc", "-s")},
    {  CTRL,             XK_Print,      spawn,             CMD("prtsc", "-y")},
    {  CTRL|SHIFT,       XK_Print,      spawn,             CMD("prtsc", "-sy")},

       DESKTOPCHANGE(    XK_1,                             0)
       DESKTOPCHANGE(    XK_2,                             1)
       DESKTOPCHANGE(    XK_3,                             2)
       DESKTOPCHANGE(    XK_4,                             3)
#define DESKTOPS  4
};

static struct button buttons[] = {
    {  MOD,    Button1,     mousemotion,   {.i = MOVE}},
    {  MOD,    Button3,     mousemotion,   {.i = RESIZE}},
};

#endif
